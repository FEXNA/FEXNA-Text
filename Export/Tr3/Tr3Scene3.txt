##Temple background
#Urban Jungle
#Crane mid-right, Stephen far-left
\wait[12]\f[5|Crane]\f[1|Stephen]\fc[1|Member]\wait[4]
#Stephen
\s[1]
Mister Crane?|
#Crane
\s[5]
What is it,\w[7] Stephen?|
#Stephen
\s[1]
Sorry to disturb you,\w[8] but one of our\n
sentries reportedly sighted a Bernite\n
flying near the southern range.|\n
It could be nothing,\w[5] a lost patrol,\w[4] say...|
#Crane
\s[5]
Were it nothing,\w[9] you wouldn't\n
have been so foolish as to\n
waste my time with it,\w[6] yes?|
#Stephen
\s[1]
Never,\w[7] sir.|
#Crane
\s[5]
Very good.\w[17] Bring me fresh candles\n
and lock the panic room.\w[8] I leave the\n
command in your capable hands.|
#Stephen
\s[1]
Still bookkeeping,\w[6] sir?|
#Crane
\s[5]
Indeed.\w[14] That Lycian upstart has certainly\n
made it difficult for a man to turn a profit.|\n
Our plants are reluctant to so much as\n
report in,\w[7] and many of our operations have\n
shuddered to a halt.| I hear he has even\n
rallied a strike force and taken for Etruria,\w[9]\n
to hunt out and destroy all we have created.|\n
It's enough to give a man an ulcer.|
#Stephen
\s[1]
It's awful,\w[8] Mister Crane.|
#Crane
\s[5]
I just said that,\w[9] didn't I?\w[14] Why\n
are you still here?\w[11] You have\n
a breach to attend to.|
#Stephen
\s[1]
Of course,\w[6] sir.\w[12]\n
At once.|

\s[nil]\wait[4]\f[1|nil]\wait[6]

#Crane smiles
\e[5|1]
#Crane
\s[5]
Are you coming for me,\w[5] Lord\n
Uther?\w[18] Am I to be the next to\n
suffer your legendary wrath?|\n
You might find I'm not so easily cowed.|