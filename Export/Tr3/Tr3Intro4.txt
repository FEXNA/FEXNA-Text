##Mountain background
\g[Mountains]
\loc[Bernese Ranges]
#Eliza mid-left, Melanie close-right, Brendan far-right
\wait[32]\f[2|Eliza]\f[4|Melanie]\f[6|Brendan]\wait[4]
#Ambience (rain)?
#Melanie
\s[4]
It's a tight contract, f'sure,\w[6] but...\w[9]\n
I dunno,\w[6] I got this feelin'.|\n
Lycian noblemen have less t' occupy\n
themselves with than Etrurians -\w[9] they\n
have more time t' get up ta trouble.|
#Brendan sneers
\e[6|2]
#Brendan
\s[6]
You're being paranoid.\w[10] Everything's here,\w[6]\n
and the report was as comprehensive as\n
any we've ever gotten.|\scroll
#Brendan normal
\e[6|0]
Besides,\w[7] the boss has\n
already made her mind up.|
#Melanie
\s[4]
Just 'cos we're on th' precipice,\w[7]\n
don't mean it's too late t' turn back.|
#Eliza
\s[2]
Quiet,\w[6] please.\w[14] Brendan,\w[6]\n
would you get the others?|
#Brendan
\s[6]
Yes,\w[6] boss.|

\s[nil]\wait[4]

#Brendan exits

\f[6|nil]
#Melanie
\s[4]
Someone's coming...\w[6]\n
Do you think--|
#Eliza
\s[2]
Shh.|

\s[nil]

#String 3
#Melanie shifts far-left, Eagler shifts far-right
\f[4|nil]\wait[12]\f[1|Melanie]\wait[20]\f[6|Eagler]
#Eagler
\s[6]
Well met.\w[11] I am Sir Eagler,\w[6]\n
knight of Caelin.\w[10]\n
Who might you be?|
#Eliza
\s[2]
Commander Eliza of the\n
Silent Hunters.\w[8]\n
P-\w[2]pleased to meet you.|
#Eagler
\s[6]
Very good.\w[14] Please wait here\n
while I summon my lady.|
#Eliza
\s[2]
Of course.|

\s[nil]\wait[6]

#Eagler exits
\f[6|nil]
#Melanie
\s[1]
...Pretentious twat.|
#Eliza angryface
\e[2|2]
#Eliza
\s[2]
Silence, before you get us b-\w[2]\n
get us both killed.| We don't know\n
what sort of emp-\w[2]employer we're\n
dealing with here.|

\s[nil]

#Eliza normal
\e[2|0]\wait[18]
#Madelyn shifts mid-right, Hassar shifts far-right
\f[6|Hassar]\f[5|Madelyn]\wait[8]
#Madelyn smiles
\e[5|1]
#Madelyn
\s[5]
Hello there!\w[12] I'm Madelyn,\w[9] and\n
this is Hassar,\w[6] and we're\n
going to be your charges!|\n
Isn't it exciting~?|
#Eliza
\s[2]
...I supp...\w[2] I suppose that\n
answers that question.|
#Hassar
\s[6]
You build a tolerance\n
over time.|

\s[nil]\wait[8]\f[1|nil]\f[2|nil]\f[5|nil]\f[6|nil]

#still String 3
#Eliza mid-left, Darin close-right, Hassar mid-right, Eiry far-right
\wait[45]\f[2|Eliza]\wait[6]\f[4|Darin]\f[5|Hassar]\f[6|Eiry]
#Eliza
\s[2]
Esteemed sirs,\w[7] madams,\w[6] I welcome you.|\n
I and my team are honored to be working\n
alongside such a diverse assembly of venerable\n
p-\w[2]persons,\w[7] and I vow on their behalf that we will\n
puh...\w[2] perform our duty without error.|
#Darin
\s[4]\e[4|1]
Hear, hear!|
#Eiry
\s[6]
Oh,\w[6] you just like that\n
she complimented you.|
#Darin
\s[4]
I never claimed otherwise.|\e[4|0]
#Eliza
\s[2]
The rep...\w[2] representatives for Etruria,\w[6]\n
Caelin,\w[6] Laus, and Tania are already\n
among us,\w[9] for which we are grateful.|\n
Sir Isaac of Pherae and his comp-\w[2]panions\n
have already embarked;\w[9] however,\w[6]\n
the Bernese rep...\w[2] rep...|
#Eliza
\scroll
The Bernite has not yet shown.|
#Darin smiles
\e[4|1]
#Darin
\s[4]
And who is surprised,\w[5] eh?|
#Hassar
\s[5]
Marquess Laus,\w[9] please...|
#Darin
\s[4]
Ha!\w[8] Of course the savage\n
speaks in his defense.|

\s[nil]\wait[4]

#El Hombre?
#Darin shifts close-left

\f[4|nil]\wait[6]\f[3|Darin]\e[3|1]\wait[4]
#Darin
\s[3]
Why are you even here,\w[6] eh?\w[20]\n
I have heard no mention of a\n
Sacaean force to be joining us -|\n
is it that Lady Madelyn has become\n
attached to her little pet?| Does she\n
feel the need to run its legs?|

\s[nil]\f[6|nil]\wait[6]

#Wallace shifts far-right
\f[6|Wallace]
#Wallace angryface
\e[6|2]
#Wallace
\s[6]
What did you say?|
#Darin normal
\e[3|0]
#Darin
\s[3]
Only what the respectable among us\n
have been thinking,\w[7] Sir Wallace.\w[6]\n
I warn you to mind your position.|
#Hassar
\s[5]
The way I measure it,\w[6] his position\n
is a good fifty pounds heavier than yours.|
#Eliza sadface
\e[2|3]
#Eliza
\s[2]
Gentlemen!\w[11] This is not accep-\w[2]table\n
conduct!\w[5] P-\w[2]p-\w[2]please,\w[3] focus on the\n
st-\w[2]strategy hearing...|

#Darin
\s[3]
What does a mongrel like\n
this know of strategy,\w[5] hm?|
#Hassar
\s[5]
Why not draw your weapon\n
and find out?|

\s[nil]\wait[4]

#Silence
#Eliza shifts far-left
\m[2|1]
#Eliza
\s[1]
No! Sto- stop this!|

\s[nil]\wait[6]

#Darin
\m[3|3]
\s[3]
!|
#Hassar smiles
\e[5|1]
#Hassar
\s[5]
My good friend Toni unfastened your\n
axe's head whilst you were posturing.|\n
We have been in this business for as long\n
as you have been growing facial hair,\w[7] My\n
Lord,\w[9] and we have become quite good at it.|
#Wallace normal
\e[6|0]
#Wallace
\s[6]
Ha!|
#Darin sadface
\e[3|3]
#Darin
\s[3]
...\w[3]I see.\w[14]\n
Very clever.|

\s[nil]\wait[8]

#Eiry returns, Darin shifts close-right, Hassar normal
\f[6|nil]\wait[3]\f[6|Eiry]\wait[12]
\f[3|nil]\wait[3]
\f[4|Darin]\e[4|3]\e[5|0]\o[5]\o[6]
#Darin
\s[4]
My apologies,\w[7] commander.\w[15]\n
Please continue.|
#Eliza
\s[1]
...R-\w[2]right.|

\s[nil]\wait[4]

#Eliza shifts mid-left, normal
\m[1|2]\e[2|0]
#String 9? String 3 again?
#Eliza
\s[2]
We have almost no intelligence on the\n
enemy's p-\w[2]position,\w[7] and that which we\n
have may be unreliable.| What Master\n
Leonard was able to give us is as follows:|\scroll
The kingpuh...\w[2] kingpin of the op-\w[2]operation\n
is a very security-conscious man.|\n
He commands the entire organization from\n
this p...\w[2] from this fortress,| and no one enters or\n
leaves its walls without being p-\w[2]personally\n
identified by himself or his direct attendants.|
#Eiry
\s[6]
Well,\w[8] we're obviously\n
not here to sneak in.|
#Eliza
\s[2]
Indeed.\w[12] Lord Uther believes a direct siege\n
is the best app...\w[2] approach.| They have\n
well-trained ballisticians in their emp-\w[2]employ,\n
but only a small handful of the machines.|\n
The cap-\w[2]captive leader described several\n
regiments of cavalry,\w[7] elite swordsmen and\n
ex-Ilian irregulars,\w[9] as well as a robust staff\n
of support personnel.|
#Darin normal
\e[4|0]
#Darin
\s[4]
So,\w[7] what then is the strategy?\w[13]\n
Charge through the gates and try\n
to kill more of them than they do us?|
#Eliza smiles
\e[2|1]
#Eliza
\s[2]
Is not that your sp-\w[2]specialty,\w[8]\n
Lord Darin?|
#Darin smiles
\e[4|1]
#Darin
\s[4]
Ha!\w[10] I suppose it is,\w[9]\n
commander.|
#Eliza normal
\e[2|0]
#Eliza
\s[2]
It is no secret that this base is incredibly\n
well-defended,\w[6] and that this mission\n
has a high likelihood of fatality.|
#Eiry
\s[6]
An optimistic way to consider it is that our\n
deaths would inspire a more considerable\n
effort from our respective homelands.|
#Eliza
\s[2]
Er...|
#Darin
\s[4]
Forget dying,\w[9] I say we\n
get it right the first time!|
#Hassar smiles
\e[5|1]
#Hassar
\s[5]
...Hear, hear.|