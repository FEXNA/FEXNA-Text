\f[5|Madelyn]
\s[5]\e[5|1]
Hey!\w[15] This chapter doesn't normally have\n
preparations,\w[5] but you'll need it to organize\n
your items.|\e[5|0] You won't be able to reposition\n
your units,\w[5] though.\e[5|1]\w[10] Sorry!|