# Uther mid-right, Eiry mid-left
\f[2|Eiry]\f[5|Actor1]

# Uther
\s[5]
Hello,\w[6] my dear.\w[16] Are you in\n
the middle of something?|
# Eiry
\s[2]
Oh,\w[5] Lord Uther!\w[18] Nothing of import;|\n
# Eiry smiles
\e[2|1]
and yet,\w[6] it is hard to conceive of\n
anything I would rather do than\n
speak with you.|
# Uther smiles
\e[5|1]
# Uther
\s[5]
You disarm me,\w[7] my fair princess.|\n
I had come with intent of flattering\n
you into submission,\w[7] and instead it is\n
me blushing like a virgin maid.|
# Eiry
\s[2]
Somehow I sense this\n
weakness is feigned.|
# Uther
\s[5]
Heh,\w[5] perhaps.\w[16] Come, then,\w[3] let us find\n
each others' true vulnerabilities.|\n
There is much I would know about\n
your work,\w[6] and your superiors.|
# Eiry
\s[2]
I would rather we keep\n
telling each other sweet lies.|

# Uther shifts close-right
\s[nil]\wait[4]\m[5|4]\wait[6]

# Uther
\s[4]
Shall I compare you to a\n
shining saint from on high?|
# Eiry
\s[2]
And what does that make you,\w[4]\n
then?\w[14] Some imp from the under-\n
world,\w[5] come to steal me away?|
# Uther
\s[4]
I'm not sure I appreciate\n
that likeness.\w[16] Perhaps\n
something more imposing?|
# Eiry
\s[2]
You do have something of\n
the dickens about you.|\n
Yes,\w[6] I think you would make\n
for a fine djinn,\w[6] or...\w[21]\n
Perhaps an incubus?|
# Uther normal
\e[4|0]
# Uther
\s[4]
To be perfectly honest,\w[6] I've\n
lost track of this conversation.|
# Eiry normal
\e[2|0]
# Eiry
\s[2]
Oh,\w[5] I apologize.\w[16] The incubus is a\n
handsome,\w[6] seductive fiend,| said to\n
steal his way into women's beds and\n
plant the seed of evil inside them.|
# Uther smiles
\e[4|1]
# Uther
\s[4]
I'm afraid playing into such\n
a role would destroy what\n
tattered honor I have left...|
# Eiry smiles, squints
\e[2|1]\b[2|1]
# Eiry
\s[2]
I won't tell if you don't~|
# Uther normal
\e[4|0]
# Uther
\s[4]
Hold a moment.\w[19] You've completely\n
diverted me from my course,\w[5] here...|
# Eiry closes eyes
\b[2|2]
# Eiry
\s[2]
I have no idea what you are\n
talking about,\w[6] my lord.|\n
# Eiry opens eyes
\b[2|0]
In any case,\w[6] I must return\n
to my rituals if I would be\n
of any use to our cause.|
# Uther jumps, smiling
\e[4|1]\m[4|4]
# Uther
\s[4]
You're doing it again!\w[14] Do not\n
think this matter forgotten,\w[6]\n
my devious, little angel.|
#Eiry
\s[2]
Of course not,\w[5] my\n
big, dark prince.|