# Zephyhr mid-left, Melanie mid-right
\f[2|Zephyr]\f[5|Melanie]

# Melanie
\s[5]
Augh,\w[6] when are we going to\n
get some meat on this team?\w[19]\n
# Melanie jumps
\m[5|5]
I'm starving!|
# Zephyr
\s[2]
Somehow,\w[6] I don't think\n
you're talking about steak.|
# Melanie
\s[5]
Hell yes,\w[6] I am.\w[17] I want some\n
grade-A, red, hot, juicy beefcake.|\n
I thought it would be great riding\n
with a group of women,\w[5] and it is,\w[5]\n
but a girl's got needs.|
# Zephyr
\s[2]
What about Brendan?\w[19]\n
He's pretty "beefy".|
# Melanie
\s[5]
Been there,\w[5] done that.\w[19] I want\n
something new and exciting!|
# Zephyr
\s[2]
Wait,\w[6] you've done Bren?\w[21] How is\n
this the first I'm hearing of it?|
# Melanie
\s[5]
At least a dozen times,\w[6] yeah.\w[25]\n
He's fond o' chokin',\w[5] which is\n
nice,\w[5] but it gets old after a while.|
# Zephyr jumps
\m[2|2]
# Zephyr
\s[2]
Oh,\w[5] ew!\w[21] Suddenly I don't want\n
to be having this conversation.|
# Melanie
\s[5]
You know what I really want?\w[19]\n
Someone fair,\w[4] and thin,\w[4] and\n
sensitive.\w[21] Someone I can RUIN.|
# Zephyr
\s[2]
Yeah,\w[3] that's about what I would\n
expect from you.\w[19] I always thought\n
you liked women,\w[6] to be honest.|
# Melanie
\s[5]
Girls are fun,\w[6] but they're like\n
snacks.\w[21] They don't really satsify.|\n
Sometimes you need someone t'\n
go so deep that you're bow-legged\n
for th' next two days.|
# Zephyr
\s[2]
You're so gross.|
# Melanie
\s[5]
Don't pretend like y' don't know\n
what I'm talkin' about.| Our rooms\n
are adjacent,\w[6] I can hear you\n
punishing yourself late at night.|
# Zephyr jumps, angry
\m[2|2]\e[2|2]
# Zephyr
\s[2]
Mel!\w[26] Gods, I am done\n
with you today.|

# Zephyr exits
\s[nil]\wait[4]\f[2|nil]

# Melanie
\s[5]
Y' can't run from your vagina,\w[6]\n
Zeph!\w[19] Sooner or later you're\n
gonna have to give in to it!|