# Marcus mid-left, facing left
\r[2]\f[2|Marcus]\wait[32]

# Uther
\name[7|Uther]\s[7]
Hey,\w[5] there!|

# Marcus turns, Uther mid-right
\s[nil]\wait[8]\r[2]\wait[6]\f[5|Actor1]\wait[12]

# Marcus
\s[2]
Are you addressing\n
me,\w[4] my lord?|
# Uther
\s[5]
Yeah,\w[5] you.\w[17] How are\n
you holding up?|
# Marcus
\s[2]
Oh,\w[4] I...\w[9] I believe that I am doing\n
Pherae proud,\w[5] sire.|\n
I must say,\w[6] however,\w[6] it is strange\n
to see these battles unfold before\n
me so unlike they would on paper.|\n
There are some factors my studies\n
did not account for,\w[6] it seems,\w[6] and\n
so I must alter my drills...|
# Uther
\s[5]
Is that what you're\n
doing there?|
# Marcus
\s[2]
Yes,\w[6] my lord.\w[18] These brief respites\n
are precious,\w[5] and I cannot waste\n
them if I am to stay in peak condition.|
# Uther
\s[5]
That's great,\w[6] kid.|
# Marcus
\s[2]
...Respectfully,\w[6] sire,\w[6] I am at\n
least a full year your senior.|
# Uther
\s[5]
Huh.\w[15] No kidding?|
# Marcus
\s[2]
I am not a man known\n
for jests,\w[5] my lord.|
# Uther squints
\b[5|1]
# Uther
\s[5]
Yeah,\w[6] I'm beginning\n
to get that.|
# Marcus
\s[2]
If I may,\w[6] my lord,\w[6] there\n
is another thing...\w[21]\n
My name,\w[5] sire,\w[5] it's--|
# Uther opens his eyes
\b[5|0]
# Uther
\s[5]
Oh,\w[4] there she is.\w[11]\n
Excuse me.|

# Uther shifts offscreen left
\s[nil]\wait[12]\m[5|0]\wait[3]\f[0|nil]\wait[12]

# Marcus
\s[2]
It's Sir Marcus.\w[16]
# Marcus squints
\b[2|1]
 My\n
name is Sir Marcus.|