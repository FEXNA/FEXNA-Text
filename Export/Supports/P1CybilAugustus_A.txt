# Cybil mid-left, facing left, Augustus appears mid-right
\r[2]\f[2|Cybil]\wait[45]\f[5|Augustus]\wait[12]

# Augustus
\s[5]
Oh,\w[5] hello miss.\w[15] Is all\n
well with your steed?|
# Cybil
\s[2]
As far as I can see,\w[5] she's fine,\w[5]\n
but...|\n
She's in an uncharacteristically\n
stroppy mood,\w[5] today.|

# Augustus moves close-left
\s[nil]\wait[6]\m[5|3]\wait[12]

# Augustus
\s[3]
Please,\w[5] allow me...|
# Cybil
\s[2]
You sure?\w[18] Pegasi don't have\n
much patience for men-folk,| and\n
Buttercup here has been biting\n
all morning.|
# Augustus
\s[3]
I insist.|
# Cybil
\s[2]
If you say so...|

# Cybil moves close-left, Augustus moves mid-left
\s[nil]\wait[16]\m[2|3]\wait[8]

# Augustus
\s[2]
Hmm...\w[22] She does seem to be fit,\w[5]\n
but...\w[21] There's a swelling...\w[16]
# Augustus jumps
\m[2|2]
 Ah.|
# Cybil
\s[3]
Huh.\w[14] I've never seen her so\n
calm around a man before.\w[16] Do you\n
wear perfume or something?|
# Augustus smiles
\e[2|1]
# Augustus
\s[2]
Not usually,\w[4] no.\w[19]\e[2|0]\n
Look at this.|

# Cybil moves mid-left, Augustus moves close-left
\s[nil]\wait[16]\m[2|3]\wait[8]

# Augustus
\s[3]
Do you see?\w[18] There.|
# Cybil squints
\b[2|1]
# Cybil
\s[2]
What,\w[5] in her...\w[19] Oh.|\n
# Cybil opens eyes
\b[2|0]
Ohhhh.|

# Augustus moves mid-right
\s[nil]\wait[6]\m[3|5]\wait[12]

# Augustus
\s[5]
She's in heat,\w[6] is all.\w[19] It will pass,\w[5]\n
soon enough,\w[5] but for now try\n
not to make a big deal about it.|\n
She must be starting to feel\n
self-conscious from all of this\n
attention.|
# Cybil smiles
\e[2|1]
# Cybil
\s[2]
Heh.\w[18] Sorry,\w[5] honey,\w[5]\n
I was just worried.|

# Cybil turns
\s[nil]\wait[18]\r[2]\wait[12]

# Cybil
\s[2]
You are a strange fellow,\w[5] Sergeant\n
Major.|\n
Most Lycians I have met treat their\n
horses like another tool in the arsenal,|\n
and not as an ally who fights by their\n
side.|\n
The very idea of an animal being\n
frightened or embarassed would never\n
occur to them.|
# Augustus
\s[5]
Then it is my opinion that you\n
have been meeting the wrong\n
sort of Lycians.|\n
Our nation could only have\n
become what it is today due to\n
the efforts of these beasts.|\n
Horses are a part of our culture\n
and our heritage,| and should be\n
treated with the same respect\n
as the men who ride them.|
# Cybil
\s[2]
I couldn't agree more.\w[16] Perhaps\n
that is what Buttercup sensed\n
in you,| to be so comfortable in\n
your presence.|
# Augustus smiles, squints
\b[5|1]\e[5|1]
# Augustus
\s[5]
Perhaps...|\n
# Augustus opens eyes
\b[2|0]
I certainly couldn't think\n
of what else it might be.|
# Cybil normal
\e[2|0]
# Cybil
\s[2]
...?|