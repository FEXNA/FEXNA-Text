# Madelyn mid-left, angry, Roeis mid-right
\f[2|Madelyn]\e[2|2]\f[5|Roeis]\wait[12]

# Madelyn
\s[2]
Why are you watching me,\w[6]\n
you strange,\w[6] little man?|
# Roeis
\s[5]\x[+47]
Was there ever a better way\n
to die?|\n
\x[Roeis]That's all living is,\w[6] after all.\w[12]\n
Dying by pieces with each\n
exhaled breath.|
# Madelyn normal
\e[2|0]
# Madelyn
\s[2]
That's a sad way to look at things.|\n
If we do,\w[3] indeed,\w[3] die with each day,\w[6]\n
then I choose to believe we are also\n
born with each waking.|\n
Then we get to live a million lives!|
# Roeis sad
\e[5|3]
# Roeis
\s[5]
What are a million lives but\n
a million ways to suffer\n
heartbreak and tragedy?|
# Madelyn smiles
\e[2|1]
# Madelyn
\s[2]
A million ways to laugh,\w[6]\n
and love,\w[6] and learn!|\n
# Madelyn closes eyes
\b[2|2]
Try as you like,\w[5] creature,\w[5] you\n
simply cannot spoil my mood.|
# Roeis normal
\e[5|0]
# Roeis
\s[5]
That is for the best.\w[16] I can\n
only truly appreciate my\n
misery when I can compare|\n
it to your tyrannical hope.|
# Madelyn normal, eyes open
\e[2|0]\b[2|0]
# Madelyn
\s[2]
...|
\scroll
You are a spiteful thing,\w[6] are\n
you not?\w[16] I wonder,\w[4] what made\n
you so broken?|
# Roeis
\s[5]
Who says I am broken?\w[16] This\n
is the natural state of all things,|\n
and you will mature into what I am\n
when time heals your wounds.|
# Madelyn
\s[2]
Hmmmm.\w[14] No,\w[5] I don't believe that\n
at all.|\n
# Madelyn smiles, closes eyes
\e[2|1]\b[2|2]
For the fact that I exist as I\n
am is proof enough that this is\n
how I was meant to be.|\n
If I were supposed to be any\n
other way,\w[6] I would be that way,\w[5]\n
instead of how I am.|
# Roeis squints
\b[5|1]
# Roeis
\s[5]\x[Roeis]
As I said.\w[15] You are a\n
tyrant of optimism.|
# Madelyn
\s[2]
Shush.\w[15] I am going to boost your\n
self-esteem through song,| and\n
you will stand there and behave\n
yourself.|
# Roeis smiles, eyes closed
\e[2|1]\b[5|2]
# Roeis
\s[5]
...|