# Cybil mid-right, Toni appears mid-left
\f[5|Cybil]\wait[35]\f[2|Toni]

# Toni
\s[2]
Cybil!\w[18] There you are.|
# Cybil angry
\e[5|2]
# Cybil
\s[5]
Shut up!\w[18] Go away!|
# Toni
\s[2]
S'cuse me?|
# Cybil
\s[5]
As long as I'm focusing on\n
the battle,\w[5] Uther can't justify\n
instigating a conflict.|\n
But if he sees me talking to\n
you,\w[5] he's going to come\n
thundering over here!|
# Toni
\s[2]
What if I shoot while I talk?|
# Cybil
\s[5]
Fine.\w[16] But if he so much as\n
throws us a dirty look, you're\n
on your own.|
# Toni
\s[2]
Why y' gotta be like that?\w[18] I\n
reckon we should stand our\n
ground,| try t' talk him down\n
together.|
# Cybil normal
\e[5|0]
# Cybil
\s[5]
You saw his face.\w[16] That's a\n
man who takes his hair very\n
seriously.|
# Toni smiles
\e[2|1]
# Toni
\s[2]
He really does,\w[5] don't he?\w[16] He didn't\n
even get so far as usin' th' scented\n
soap.| As soon as th' dye was in,\w[5]\n
he knew somethin' was wrong.|
# Cybil angry
\e[5|2]
# Cybil
\s[5]
What's he so fussy about,\w[5]\n
anyway?| For all the brushes\n
and oils he uses on it,\w[5] it still\n
looks like a tangled mane.|
# Toni normal
\e[2|0]
# Toni
\s[2]
I s'pose he works hard t' make it\n
look exactly like that.\w[17] It's prob'ly\n
just what's in fashion in the cities.|
# Cybil normal
\e[5|0]
# Cybil
\s[5]
Maybe.\w[15] I have to admit,\w[5] it looks\n
somewhat appealing on him.\w[19]\n
# Cybil smiles
\e[5|1]
Moreso now that it's bright pink.|
# Toni smiles, eyes closed
\b[2|2]\e[2|1]
# Toni
\s[2]
Hahaha,\w[5] yes!\w[16]\n
Totally worth it!|
# Cybil normal
\e[5|0]
# Cybil
\s[5]
Shh...\wait[20] Is he looking?|
# Toni normal, eyes open
\b[2|0]\e[2|0]
# Toni
\s[2]
...|
\scroll
I don't think so.|
# Cybil squints
\b[5|1]
# Cybil
\s[5]
Hmm.|
# Toni
\s[2]
Anyway,\w[7] I mean what I said,\w[5]\n
about us stickin' together.|
# Cybil opens eyes
\b[5|0]
# Cybil
\s[5]
What are you blathering about?|
# Toni
\s[2]
You an' me!\w[18] We're as good as\n
sisters,\w[5] right?\w[14] If we work together,\w[5]\n
I'm sure we ken handle Uther -|\n
or damn near anythin'\n
that comes our way.|
# Cybil
\s[5]
We have sort of fallen in sync\n
with each other,\w[5] haven't we?|\n
I dodge,\w[6] you shoot;\w[12] I dive,\w[6]\n
you roll...\w[18] Yes,\w[5] this could work.|
# Toni
\s[2]
I don't jus' mean in battle...\w[12] When Eagler\n
found me screwin' with his belts,\w[6] he fair\n
to blew his scalp off,| but you talked him\n
down enough fer me t' get out o' there.|
# Cybil smiles
\e[5|1]
# Cybil
\s[5]
Oh,\w[5] remember when I first teased\n
Hassar about his raspy voice?\w[16]\n
How he snapped back at me?|\n
You told him I was serious,\w[5] that it\n
was a sacred thing in Ilian culture.|\n
I couldn't look him in the eye for\n
weeks,\w[5] he felt so guilty about it.|
# Toni smiles, jumps
\e[2|1]\m[2|2]
# Toni
\s[2]
Yes,\w[5] exactly!\w[18] We been coverin'\n
each other's arses fer as long\n
as we've known each other!|
# Cybil
\s[5]
Heh...|

# Uther
\s[nil]\wait[18]\name[0|Uther]\s[0]
We need to talk,\w[5]\n
you little shits.|

# Cybil normal
\e[5|0]
# Cybil
\s[5]
Sorry,\w[5] you're on your own.|

# Cybil exits, Toni moves close-right
\s[nil]\wait[12]\f[5|nil]\e[2|0]\wait[8]\m[2|4]\wait[12]

# Toni
\s[4]
Damn it,\w[5] Cybil!|

# Uther
\s[0]
Come here!|

# Toni turns
\s[nil]\wait[8]\r[4]\wait[4]\s[4]
Eep!|