#Harken mid-left, Chester mid-right
\f[2|Harken]\f[5|Chester]\wait[4]

#Harken
\s[2]
Chester?|
#Chester
\s[5]
Yeah,\w[5] buddy?|
#Harken
\s[2]
...Does it...\w[19] Does\n
it really get easier?|\n
You know,\w[6] coping\n
with your demons...|
#Chester squints
\b[5|1]\s[5]
...I never said it did,\w[5] buddy.\w[21]\n
I said you get better at coping.|
#Harken
\s[2]
But...|
#Chester normal
\b[5|0]\s[5]
It gets harder.\w[19] As time goes on,\w[5] you find\n
more reasons to be miserable and you\n
have less energy to fight.|\n
Friends disappear,\w[5] lovers scorn you,\w[5]\n
thousands die,\w[5]
#Chester squints
\b[5|1]
 and all the while you get\n
older and further away from your dreams...|
#Harken
\s[2]
...|
#Chester smiles
\e[5|1]\s[5]
Ah,\w[5] but listen to me.|\scroll
#Chester opens eyes
\b[5|0]
There's always something new around\n
the corner,\w[5] lad.| That's the important\n
thing.\w[14] You need to keep fighting the\n
demon,\w[5] keep finding ways to beat him.|\n
You'll grow stronger,\w[5] stronger than you'd\n
believe,\w[5] stronger than ANYONE,\w[9] despite\n
your exhaustion and misery.|\scroll
And come the morning,\w[5] you'll rise again,\w[5]\n
and who knows?\w[18] Maybe you'll think of a clever\n
riddle,\w[5] or see a wonder,\w[5] or meet a new friend.|\n
Maybe a friend like yourself,\w[5] who can see the fire\n
in your eyes and know what you're living through.\w[19]\n
Someone who will respect you for it.|
#Harken sad
\e[2|3]\s[2]
Ch-Chester...|
#Chester closes eyes
\b[5|2]\s[5]
I know,\w[5] little brother.\w[12]\n
I know.|