# Magnus mid-left, Eiry mid-right
\f[2|Magnus]\f[5|Eiry]

# Magnus
\s[2]
So...\w[19] You're saying that all\n
magic is based on life-force?|
# Eiry closes eyes
\b[5|2]
# Eiry
\s[5]
No,\w[7] Magnus.\w[19] That is a gross\n
oversimplification of the concept.|\n
# Eiry opens eyes
\b[5|0]
It is clear now that you have\n
been poisoned by the meddling\n
of lesser minds.|
# Magnus squints
\b[2|1]
# Magnus
\s[2]
(Damn you,\w[6] Professor White...)|
# Eiry
\s[5]
There is a power,\w[6]
# Magnus opens eyes
\b[2|0]
 called by some\n
"energy" and by some "mana" that\n
permeates every physical thing in|\n
this world.\w[21] Yes,\w[4] it is true that living\n
creatures are possessed of it,\w[7] but\n
so too are the minerals in the earth|\n
and the light that pours down from\n
the sky.|\n
It is what gives an object its weight,\w[6]\n
what binds it together,| what allows it\n
to be transformed from one thing to\n
another.|\n
When you cast a spell,\w[6] you are\n
giving a sequence of commands to\n
the energy of the things around you.|\n
You can use it to change the shape\n
of a thing,\w[6] or move it,\w[6] or transfer its\n
energy to another object.|\n
Today,\w[7] I want you to forego the use\n
of incantations and try to manipulate\n
this energy by feel and force of will.|
# Magnus
\s[2]
What,\w[6] just...\w[11] wish for\n
a fireball to appear?|
# Eiry angry, closes eyes
\e[5|2]\b[5|2]
# Eiry
\s[5]
...\w[9]You are being facetious,\w[7] but\n
yes,\w[7] that is essentially what I\n
am asking of you.|\n
# Eiry opens eyes
\b[5|0]
Recall times in the past that\n
you have summoned one using\n
an incantation,\w[6] and try to feel|\n
those same sensations.|
# Magnus
\s[2]
Hmm...|

# Magnus closes eyes, Eiry normal, time passes
\s[nil]\b[2|2]\e[5|0]\wait[60]

# Magnus
\s[2]
I think...\w[12] I can feel it,\w[7] boss.\w[19]\n
It's like...|
\scroll
A prickling heat in my bones...|\n
static under my skin...|\n
a flutter in my stomach...|
\scroll
But I am feeling these things\n
at once within my body and\n
outside of it,\w[6] as if...|\n
my body were the world itself...|
# Eiry
\s[5]
...Truly?\w[19]\n
So quickly...?|
# Magnus
\s[2]
I've...\w[11] I've always been aware\n
of it when I cast before,\w[8]\n
anyway...|\n
\b[2|1]I sort of just...\w[9] mumbled the\n
lines without thinking about\n
them,| and focused my attention\n
on the surge of heat and the\n
sudden release...|
# Eiry
\s[5]
Interesting...\w[21] A surly,\w[5] level-headed\n
pragmatist like yourself...| I had\n
thought reason would be the best\n
path for you.|\n
# Eiry smiles
\e[5|1]
But it seems,\w[6] underneath,\w[6] you\n
are quite the emotional fellow.|
# Magnus opens eyes
\b[2|0]
# Magnus
\s[2]
Phew.\w[11] I'm exhausted.\w[16]\n
And no fireballs,\w[6] it seems.|
# Eiry normal
\e[5|0]
# Eiry
\s[5]
Of course not.\w[17] Casting without\n
a tome is impossible,\w[6] or at least,|\n
it has been since the so-called\n
"Ending Winter".|\n
The purpose of this exercise was\n
to heighten your awareness of\n
the ambient energy around you.|\n
Such is the fuel that powers your\n
Anima magic,\w[5] and being able to\n
find rich sources of it and sense|\n
when your enemy is manipulating\n
it will be vital.|
# Magnus
\s[2]
I get it.\w[18] And when I am bending\n
the elements,\w[6] I will be able to\n
feel the flow of energy and|\n
correct malformations,\w[6] instead\n
of needing to pay such close\n
heed to my arcane grammar.|
# Eiry smiles, closes eyes
\e[5|1]\b[5|2]
# Eiry
\s[5]
Precisely.\w[19] Very good,\w[5] Magnus.|\n
You are mastering this with\n
remarkable ease -\w[10] and it is|\n
certain you could not have\n
heard THAT from Professor\n
White.|
# Magnus
\s[2]
What have you got\n
against him,\w[6] anyway?|
# Eiry normal, opens eyes
\e[5|0]\b[5|0]
# Eiry
\s[5]
He has no comprehension of\n
the craft and no appreciation\n
for genius.|
# Magnus smiles
\e[2|1]
# Magnus
\s[2]
He flunked you,\w[6] didn't he?|
# Eiry angry, jumps
\e[5|2]\m[5|5]
# Eiry
\s[5]
And he was humiliated when\n
the wider magical community\n
read my manifesto and came|\n
to a consensus on its merit.|
# Magnus
\s[2]
But he never apologized,\w[6] huh?|
# Eiry squints
\b[5|1]
# Eiry
\s[5]
I will have my revenge.|