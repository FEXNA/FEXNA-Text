# Eiry mid-left, Magnus mid-right
\f[2|Eiry]\f[5|Magnus]

# Eiry
\s[2]
We would seem to have a\n
moment to ourselves...|\n
Now would be a good time to\n
continue your lessons,\w[5] then.|
# Magnus
\s[5]
Whatever you say,\w[6] boss.|
# Eiry closes eyes
\b[2|2]
# Eiry
\s[2]
Now,\w[6] then.\w[18] So far,\w[6] I have been\n
teaching you to cast from rote,|\n
which is a fine thing if you want\n
stable,\w[5] well-formed spells.|\n
The standard incantations have\n
been perfected over the course\n
of centuries,\w[6] and using them allows|\n
you to harness the expertise and\n
knowledge of all the wizards who\n
came before you.|\n
However,\w[5] firing off someone else's\n
fireballs like a glorified onager\n
will get you only so far.|\n
To ascend to the next level,\w[5] you\n
must come to understand what\n
the words themselves mean,\w[5] and|\n
how to structure a spell.|
\scroll
# Eiry opens eyes
\b[2|0]
Can you think of any situations\n
where it would benefit you to\n
know such things?|
# Magnus squints
\b[5|1]
# Magnus
\s[5]
I suppose...\w[12]
# Magnus opens eyes
\b[5|0]
 If I wanted to\n
modify a spell,\w[5] to make it\n
behave differently?|\n
Say,\w[5] to make a blade of\n
wind blow hot or cold...|\n
Or to make a fireball tight\n
and focused,\w[5] in order to\n
reduce collateral damage.|
# Eiry squints
\b[2|1]
# Eiry
\s[2]
...\w[4]Those are the exact examples\n
used in a remedial spellcraft class.|\n
Have you been doing extracurricular\n
research?|
# Magnus
\s[5]
No,\w[5] boss,\w[5] it just...\w[11]\n
makes sense.|
# Eiry
\s[2]
Hmmm...\wait[21]
# Eiry closes eyes
\b[2|2]
 Well,\w[5] I would applaud you\n
for doing so,\w[5] if such were the case.|\n
As your teacher,\w[6] I do want to stay\n
informed of your progress,| but that\n
does not mean I am unwilling to let\n
you seek lessons of others.|
# Magnus
\s[5]
I--|
# Eiry angry, squints
\e[2|2]\b[2|1]
# Eiry
\s[2]
Unless it were the likes of that\n
dullard Professor White,| whose\n
witless ramblings would only harm\n
what I have done.|
# Magnus
\s[5]
...|
# Eiry looks up
\b[2|0]
# Eiry
\s[2]
But as you said,\w[6] you naturally\n
came to that conclusion of\n
your own reasoning.|
# Magnus
\s[5]
...Indeed.|
# Eiry normal
\e[2|0]
# Eiry
\s[2]
Very well.|
\scroll
Yes,\w[6] those are ways in which an\n
understanding of spellcraft can\n
be applied.| However,\w[7] getting the\n
syntax of the language correct\n
and ensuring the spell's overall|\n
arrangement of elements is\n
cohesive is of utmost importance,|\n
for otherwise the casting of it\n
could have disastrous results.|
\scroll
It is theoretically possible to write\n
a new spell from whole cloth with\n
a complete understanding of the|\n
language,\w[6] but to acquire such\n
knowledge one would need to live\n
for centuries...|
# Magnus squints
\b[5|1]
# Magnus
\s[5]
(...Was it Professor White,\w[6] or...\w[12]\n
...Blake?\w[15] ...Heights?\w[15] He did\n
seem something of a dullard...)|
# Eiry angry
\e[2|2]
# Eiry
\s[2]
Which is why it is so important that\n
a student of such advanced age\n
work that much harder to master it.|\n
# Eiry jumps
\m[2|2]
Tell me,\w[6] Magnus,\w[6] what is the\n
cardinal element of magic?|
# Magnus opens eyes
\b[5|0]
# Magnus
\s[5]
Uhh...\wait[32]\e[5|3]\n
I don't know,\w[6] mistress.|
# Eiry closes eyes
\b[2|2]
# Eiry
\s[2]
So perhaps it might be wise\n
of you to pay attention.|
# Magnus squints
\b[5|1]
# Magnus
\s[5]
Yes,\w[5] mistress.|