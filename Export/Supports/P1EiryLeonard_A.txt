# Leonard mid-left, Eiry mid-right, facing right
\f[2|Leonard]\r[5]\f[5|Eiry]

# Leonard
\s[2]
Come now, you have to tell me.|\n
If what you're saying is true,\w[6]\n
then this has world-changing\n
implications.|\n
The Council of Seers don't\n
concern themselves with the\n
weather or with petty|\n
matchmaking...|\n
They claim to watch over the\n
course of human history itself,|\n
the rise and fall of entire\n
generations.|
# Eiry closes eyes
\b[5|2]
# Eiry
\s[5]
I don't see how that changes\n
things one whit.\w[16] This is an\n
Etrurian secret,| one I am not\n
authorized to divulge.|
# Leonard jumps
\m[2|2]
# Leonard
\s[2]
But you've already...\w[19] Blast it.|\n
How about this,\w[6] then.\w[15] I will make\n
guesses as to its nature,\w[6] and you\n
can tell me when I'm wrong.|\n
You don't even have\n
to confirm anything.|
# Eiry squints
\b[5|1]
# Eiry
\s[5]
...|
# Leonard
\s[2]
So,\w[6] we've established that the\n
Council has foreseen something,\w[6]\n
and that your government has|\n
reason to believe they are correct\n
about it.|\n
Which means that your own\n
psychics have corroborated their\n
findings...|
# Eiry closes eyes
\b[5|2]
# Eiry
\s[5]
I said no such thing.|
# Leonard smiles
\e[2|1]
# Eiry
\s[2]
...Or that you have found\n
some physical proof of\n
their claims.|
# Eiry smiles, squints
\e[5|1]\b[5|1]
# Eiry
\s[5]
...|
# Leonard
\s[2]
Okay.\w[18]
# Leonard normal
\e[2|0]
 So I would then assume\n
it has something to do with\n
Solomon's bogeyman in Bern.|
# Eiry normal
\e[5|0]
# Eiry
\s[5]
Well...|
# Leonard
\s[2]
...But not directly?\w[16]\n
Hm.\w[11] Interesting.|
# Eiry opens eyes
\b[5|0]
# Eiry
\s[5]
...|
# Leonard
\s[2]
So...\w[12] That would mean...\w[30]\n
# Leonard squints
\b[2|1]
Hmm.\w[20] Perhaps,\w[6] then...|

# Eiry turns
\s[nil]\wait[6]\r[5]\wait[12]

# Leonard
\s[2]
...Could it be that...\w[16] the threat\n
in Bern,\w[4] whether real or not...\w[9]\n
is a misdirection?|\n
And some outside influence,\w[4]\n
whatever that may be,| wants\n
us squabbling amongst ourselves\n
while it moves unseen...?|\n
And then,\w[6] that would mean...|\n
# Leonard opens eyes
\b[2|0]
There is a very real threat to\n
our world,\w[6] and our continued\n
infighting will bring it about.|
# Eiry
\s[5]
Remarkable...|
# Leonard
\s[2]
What proof did your\n
people find,\w[6] then?|
# Eiry
\s[5]
Our scryers have seen an agent\n
at work,\w[6] of unknown origin.|\n
They are elusive,\w[6] and clever,\w[6] and\n
their appearance is ever-changing.|\n
They are well-versed in the oldest\n
of magics,\w[6] the kind not seen since\n
the dawn of time...|
# Leonard
\s[2]
If ever there was a reason\n
to distrust everyone I met...|
# Eiry
\s[5]
Indeed...|