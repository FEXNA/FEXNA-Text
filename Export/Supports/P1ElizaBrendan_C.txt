# Eliza mid-left, Brendan mid-right
\f[2|Eliza]\f[5|Brendan]\wait[4]

# Brendan
\s[5]
Are you kidding?\w[21] I've told you\n
a thousand times,\w[6] she's mad.|\n
And not the fun kind of mad\n
that you invite to parties,| she's\n
the kind that wakes you up in\n
the middle of the night and|\n
demands that you cut off\n
your manhood to prove your\n
devotion to her.|
# Eliza
\s[2]
What were you even doing,\w[6]\n
getting yourself involved\n
with someone like that?|
# Brendan
\s[5]
I don't know.\w[17] She was passionate.|\n
We would fight about everything,\w[5]\n
but somehow it only made me more\n
madly in love with her.|\n
It took me many long years to\n
realize how unhealthy it was to\n
be with her,| and how bad she\n
was for our kids.|
\scroll
So no,\w[5] I'm not okay with sending\n
the boys back into that viper pit.\w[19]\n
Not even a little.|
# Eliza
\s[2]
What about your friend,\w[3] Jan?\w[14]\n
You've talked about p-p-\w[6]putting\n
them in his care before.|
# Brendan
\s[5]
Yeah,\w[5] if I'm killed in action or\n
taken prisoner.|\n
He's working full time with the\n
Green brothers,\w[6] waging war on\n
the gangs of Bern.|\n
They wouldn't be safe with him,\w[6]\n
and besides,\w[6] he has a meek\n
disposition.| The boys would drive\n
him to a nervous breakdown.|
# Eliza
\s[2]
Too much of their mother\n
in them,\w[6] hm?|
# Brendan
\s[5]
Something like that.|
# Eliza
\s[2]
So there is no one else?\w[21] No\n
aunts,\w[3] or sisters,\w[5] or rich clients\n
who owe you a debt?|
# Brendan
\s[5]
No.\w[21] Why do you want to be\n
rid of them so suddenly?|\n
You've always been supportive\n
of my keeping them with us.|
# Eliza angry
\e[2|2]
# Eliza
\s[2]
Linus keeps pissing all over\n
the floor in the privy.|\n
When I confronted him about\n
it,\w[6] he blamed his brother,\w[6] and\n
when I told him I was certain|\n
it was him,\w[6] he gave me the\n
fig and ran off.|
# Brendan smiles
\e[5|1]
# Brendan
\s[5]
Snrk.|
# Eliza squints
\b[2|1]
# Eliza
\s[2]
Is something funny,\w[5]\n
Brendan?|
# Brendan
\s[5]
No,\w[4] ma'am.\w[17] Khhk.\w[19]\n
Nothing funny about it.|
# Eliza normal, opens eyes
\e[2|0]\b[2|0]
# Eliza
\s[2]
"Too much of their mother\n
in them,"\w[6] indeed.|