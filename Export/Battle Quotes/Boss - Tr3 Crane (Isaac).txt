##Boss Quote: Crane (Isaac)
\f[1|Crane]\f[6|Isaac]\wait[12]
#Softly With Grace
#Crane smiles
\e[1|1]
#Crane
\s[1]
Well now,\w[7] what an occasion!\w[15]\n
The illustrious Sir Isaac--|
#Isaac
\s[6]
Silence.|
#Crane
\s[1]
Such hostility!\w[13] I had heard the Knights\n
of Pherae were world-renowned for their\n
chivalry and decorum.\w[10] Perhaps I was--|
#Isaac
\s[6]
I'm going to kill you now.|
#Crane normal
\e[1|0]
#Crane
\s[1]
Oh dear.|