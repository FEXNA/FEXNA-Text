##Boss Quote: Crane (Eiry)
\f[1|Crane]\f[6|Eiry]
#Softly With Grace
#Eiry
\s[6]
A fellow arcanist,\w[9] it seems.\w[14] An\n
accomplished one,\w[7] at that.\w[12] My\n
respects,\w[9] sir.|
#Crane
\s[1]
Oh?\w[12] We have ourselves a\n
budding enthusiast,\w[5] hmm?|
#Eiry
\s[6]
I'm hurt!\w[11] A journeyman,\w[6] at the least.\w[10]\n
Some say a prodigy.|
#Crane
\s[1]
Ha!\w[18] I doubt that.\w[15] All I see is\n
a painted imp,\w[9] jumped up from\n
the pits with a handful of cinders.|\n
You have no business standing\n
before a true demon!|
#Eiry angry
\e[6|2]
#Eiry
\s[6]
Why is it always with the infernal imagery?\w[14]\n
It's flea-bitten inbreds like you\n
who make it so hard for the rest of us!|