##Boss Quote: Stephen (Eliza)
\f[1|Stephen]\fc[1|Member]\f[6|Eliza]
#Rise to the Challenge
#Eliza
\s[6]
You command a stout defense,\w[9]\n
friend,\w[11] but it is not enough.\w[15] Surrender.|
#Stephen
\s[1]
I'll not capitulate to intimidation,\w[11] "friend".\w[12]\n
You've yet to overcome me,\w[8] and frankly,\w[9]\n
I don't expect you will.|
#Eliza angry
\e[6|2]
#Eliza
\s[6]
Then you are a fool.\w[14]\n
P-\w[2]prepare yourself.|
#Stephen
\s[1]
As you say.|