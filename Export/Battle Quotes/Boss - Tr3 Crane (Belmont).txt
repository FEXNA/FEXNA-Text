##Boss Quote: Crane (Belmont)
\f[1|Crane]\f[6|Belmont]\e[6|1]
#Softly With Grace
#Belmont
\s[6]
Hey.\w[12] Nice fort.|
#Crane
\s[1]
Why do I care what you think?\w[14]\n
Who are you,\w[7] to address me so?|
#Belmont normal
\e[6|0]
#Belmont
\s[6]
Jeez,\w[8] sorry.\w[16] Just trying to\n
make conversation.|
#Crane
\s[1]
We stand opposed in this\n
battle,\w[8] you pox-ridden dolt.\w[16]\n
Shut up and die!|

\m[6|6]\wait[8]\m[2|1]\wait[14]

#Belmont
\s[6]
Yikes!\w[14] No time for pleasantries,\w[7] huh?|
\wait[4]\m[1|1]\wait[12]
#Crane goes crazy
\e[2|2]
#Crane
\s[2]
Rraaaargh!|