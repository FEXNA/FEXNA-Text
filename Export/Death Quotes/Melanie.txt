\f[1|Melanie]

# Melanie closes eyes
\b[1|2]
# Melanie
\s[1]
What the fuck are you doin',\w[6] gettin'\n
yerself killed in a place like this?\w[20]\n
You were meant for better than this.|

\s[nil]\wait[23]

# Melanie squints
\b[1|1]
# Melanie
\s[1]
Oh,\w[5] I had such big plans.\w[19]\n
I was going to take it all...|

\s[nil]\wait[23]

# Melanie closes eyes
\b[1|2]
# Melanie
\s[1]
...|
\scroll
No.|
# Melanie opens eyes
\s[nil]\wait[3]\b[1|0]
# Melanie
\s[1]
Not while I got the will remainin'\n
to move my legs.\w[21] I'll beat this,\w[6]\n
like everything before it.|