\f[1|Hyde]

# Hyde
\s[1]
It is a grim day,\w[6] indeed,\w[6] when a\n
servant of the light such as myself\n
can be brought to their knees by|\n
such trifling foes...|\n
# Hyde squints
\b[1|1]
But like the Saint Elimine in her ordeal\n
at Missur,\w[7] when beset on all sides by\n
the beasts of the wild land,| I will put\n
my faith in the power of light and\n
give up all petty resistance,| and\n
instead accept that darkness must\n
fall for the sun to rise...|\n
# Hyde closes eyes
\b[1|2]
As it must,\w[7] and surely will,\w[7] for the\n
world to turn and the passage of\n
time to continue.\w[14] Join me now in song...|