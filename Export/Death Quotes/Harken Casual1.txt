\f[1|Harken]

# Harken
\s[1]
Heh...\w[15] I made this same goof\n
on my first day of service.\w[19]\n
Guess I'm pretty nervous.|\n
Better get out of here\n
before it costs me my life.|