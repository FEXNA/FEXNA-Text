\f[1|Madelyn]\f[6|Enemy]
# Madelyn
\s[1]
No! I will not tolerate-\w[8]\n
S-\w[8]stop! Get awa-\w[12] help!\w[8]\n
Help me!|
# Enemy
\m[6|5]\s[5]
You're not going anywhere,\n
little lady.\w[8] I-\wait[12]\m[5|5] hrk!|

\s[nil]\f[5|nil]\w[35]\f[5|Hassar]

# Hassar
\s[5]
...Calm down.|
# Madelyn
\s[1]
H-Hassar?|
# Hassar
\s[5]
We're all here for you.\n
Get to the rendezvous point.\n
We'll get you when it's safe.|
# Madelyn
\s[1]
Okay...|