\f[1|Cybil]

# Cybil angry
\e[1|2]\wait[6]
# Cybil
\s[1]
By Barigan's beard,\w[8] it's chaos out here!|\n
I'm spending as much time avoiding the\n
wild swings of these untrained bloody\n
recruits as I am the enemy's attacks!|\n
# Cybil normal
\e[1|0]
Unprofessional,\w[6] it is.\w[18]
# Cybil angry, jumps
\e[1|2]\m[1|1]
 No,\w[5] disgraceful!|\n
I'm withdrawing;\w[6] I hope you lot have\n
sorted this shit out when I come back!|