\r[1]\f[1|Gado]

# Gado
\s[1]
Hold on,\w[5] just--|

# Gado turns
\s[nil]\wait[2]\r[1]\wait[2]

\s[1]
I can't...\w[19] I'm all...|

# Gado leaves
\s[nil]\wait[2]\f[1|nil]\wait[2]

# Gado
\name[-1|Gado]\s[-1]
Oof.\w[19] Yeah this is better.\w[16] I'll just...\w[19]\n
stay down here 'til the world\n
stops spinnin'...|