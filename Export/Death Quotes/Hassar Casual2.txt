\f[1|Hassar]

# Hassar closes eyes
\b[1|2]
\s[1]
...|\scroll
# Hassar squints
\b[1|1]
I'm no warrior...\w[18] But...\w[26]\n
# Hassar opens eyes
\b[1|0]
I am my father's son.\w[26]\n
I should be capable of this...|

\s[nil]\wait[8]

# Hassar squints
\b[1|1]
\s[1]
...|