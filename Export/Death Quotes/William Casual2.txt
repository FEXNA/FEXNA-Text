\f[1|William]

# William squints
\b[1|1]
# William
\s[1]
I was a fool to think I could keep\n
up with these professionals...|

\s[nil]\wait[24]

# William opens eyes
\b[1|0]
\s[1]
No,\w[7] I mustn't lose heart.\w[16] I will just\n
have to try again some other day.|