\f[1|Gado]

# Gado
\s[1]
Goodness,\w[5] that was a blow and a half!\w[16]\n
Knocked the wind right out o' me!|\n
Hold on,\w[5] where's my axe...?\w[17] I lost it when\n
I fell...\w[19] It's gotta be here somewhere...\w[32]\n
# Gado sad
\e[1|3]
Oh, man,\w[5] I am in so much trouble...|