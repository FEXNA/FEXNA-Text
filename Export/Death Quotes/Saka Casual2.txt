\f[1|Saka]

# Saka
\s[1]
Ah...\w[16] Even I can tell that I'm\n
doing something wrong.\w[18] Hm.|
\scroll
Maybe next time I should...\w[17]\n
Reverse grip?\w[16] Would that...?|\n
Ugh,\w[6] why is swordplay so hard?!|