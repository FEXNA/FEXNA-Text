\f[1|Eagler]
# Eagler
\s[1]
Harumph!\w[9] I have weathered\n
far fiercer storms than this!\w[16]\n
Charge,\w[6] I say!\w[14] Chaaaarge!|

\s[nil]\f[1|nil]\wait[12]

# Offscreen right
\name[7|Eagler]\s[7]
Oof...\w[16]\n
I seem to have fallen...|