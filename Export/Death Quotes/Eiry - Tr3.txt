\f[1|Eiry]
# Eiry
\s[1]
Uhn...\w[4] I should have stayed back\n
in the tent,\w[3] huh?\w[7] Look at the state\n
I'm in,\w[7] covered in mud and bleeding... |\n
It's just unbecoming...|