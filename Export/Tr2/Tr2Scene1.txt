# Uther mid-right, Hassar far-right, Leonard mid-left, Cybil far-left
\f[5|Uther]\f[6|Hassar]\f[2|Leonard]\f[1|Cybil]

# Uther
\s[5]
Everyone's in position?|
# Cybil
\s[1]
Everyone except Bennet.\n
Fall in, you rat!\w[5]\n
Bloody conscripts.|
# Uther
\s[5]
Good enough. We hit hard and don't stop until\n
I can sit upon that throne and rest my feet on\n
their commander's corpse. Understand?|
# Leonard
\s[2]
Just give the--|