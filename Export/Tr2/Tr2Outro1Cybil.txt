# Hyde down, Roeis down or not recruited, Magnus down, Bennet down
# Uther
\s[4]
I'm not looking for one.\w[16] How does\n
twelve thousand suit you?|
# Cybil
\s[2]
Fits like a glove, m'lord.\w[12]\n
I'll trouble you no further.|
# Uther
\s[4]
A thousand thanks.|
# Cybil
\s[2]
Come,\w[8] Fargus.\w[16] Drink's on me, it seems.|
# Fargus
\s[1]
You know what I like to hear.\w[8] May\n
lady Fate bring you mercy, Uther.|
# Uther
\s[4]
Mm.|