#Castle background
\g[Castle]
#Uther close-right, Boston mid-left, Fargus far-left
\f[4|Uther]\f[2|Boston]\f[1|Fargus]
\e[4|2]\b[2|2]
#Uther
\s[4]
Stand, you cur!\w[8] Or have we knocked\n
the man out of you already?|
#Boston
\s[2]
...Haah...|
#Uther
\s[4]
Stand!\w[8] I fucking told you to--|
#Wallace, offscreen
\name[0|Wallace]\s[0]
My lord.|
#Uther
\s[4]
...Blast it all.|

#Boston, Fargus exits
\s[nil]\wait[8]\f[2|nil]\f[1|nil]

#Leonard shifts far-left, Wallace shifts mid-left
\wait[12]\f[1|Leonard]\f[2|Wallace]
#Wallace
\s[2]
If I'm not speaking out of turn,\w[8] the man\n
will need his brains\w[12] inside\w[26] his skull for\n
us to make use of them.|
#Uther
\s[4]
Wise counsel,\w[8] as always.\n
He's yours, Leonard.|
#Leonard
\s[1]
My lord is too kind.|
#Uther
\s[4]
Shut your mouth and do your\n
job.\w[12] It has been a long day.|
#Leonard
\s[1]
Aye, aye.|
#Wallace
\s[2]
Lady Madelyn expects you\n
in the gardens,\w[8] m'lord.|
#Uther
\s[4]
...\w[16]\e[4|0]\w[4]
Of course she does.\w[12] Tell her I'll be a\n
moment.\w[8] I need... to freshen up.|
#Wallace
\s[2]
My lord.|

#Leonard and Wallace exit
\s[nil]\wait[8]\f[1|nil]\f[2|nil]

#Fargus shifts far-left, Cybil shifts mid-left
\wait[12]\f[1|Fargus]\f[2|Cybil]
#Fargus
\s[1]
Y'sounded just like one'o me boys\n
back there,\w[8] Uther.\w[8] I'm prouder fer it.\n
Ain't no dignity in serving a woman.|
#Cybil
\s[2]
\m[2|2]Hmph.|
#Fargus
\s[1]
Ar, like y' count.\w[8] Ain't no man\n
ever set foot on me ship with a\n
tongue as blue as yers,\w[32] ma'am.|
#Uther
\s[4]
Can we get on with this?\w[12]\n
I ache for an ale.|
#Cybil
\s[2]
Well said.\w[8] Hassar is busy\n
doting on that unbearable princess,\w[12]\n
so do not expect a bargain.|