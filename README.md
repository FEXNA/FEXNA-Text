# FEXNA Text

Text content for [FEXNA](https://gitlab.com/FEXNA/FEXNA-Content) that contains system text as well as gameplay text for the sample chapters.

## Setup & Usage

This repo is a submodule of FEXNA, and will be automatically included at `src/Data/Text/` when submodules are initialized, or it can be downloaded manually.

## Licensing

Systems text is licensed under [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/) and is free to modify or distribute for any purpose without attribution. See full license for more details.

Other sample chapter and gameplay related text is (c) FE7x development team. Do not distribute and only use for educational purposes. All rights reserved.
